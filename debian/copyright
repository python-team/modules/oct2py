Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: oct2py
Source: http://blink1073.github.io/oct2py/docs/

Files: *
Copyright: 2012 - 2015 Steven Silvester <steven.silvester@ieee.org>
License: MIT

Files: oct2py/utils.py
Copyright: 2008 - 2015 Peter Jurica (juricap.pip.verisignlabs.com)
           2008 - 2015 RIKEN Brain Science Institute
License: BSD-3
Comment: Function get_nout at oct2py/utils.py uses code from the ompc project.
         This copyright applies where oct2py upstream used ompc code inside
         oct2py/utils.py.

Files: oct2py/core.py
Copyright: 2003-2009 Alexander Schmolck <A.Schmolck@gmx.net>
           2003-2009 Vivek Rathod
License: MIT
Comment: Function _make_octave_command at core.py uses code from mlabwrap.
         This copyright applies where oct2py upstream used mlabwrap code inside
         oct2py/core.py

Files: debian/*
Copyright: 2015 Josue Ortega <josueortega@debian.org.gt>
License: MIT

Files: oct2py/ipython/octavemagic.py
Copyright: 2008-2014, IPython Development Team
           2001-2007, Fernando Perez <fernando.perez@colorado.edu>
           2001, Janko Hauser <jhauser@zscout.de>
           2001, Nathaniel Gray <n8gray@caltech.edu>
License: BSD-3

License: MIT
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.

License: BSD-3
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 .
 Redistributions of source code must retain the above copyright notice,
 this list of conditions and the following disclaimer.
 .
 Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution.
 .
 Neither the name of the IPython Development Team nor the names of its
 contributors may be used to endorse or promote products derived from this
 software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
 FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.